<?php

namespace App\Controller;

use App\Entity\Comentarios;
use App\Entity\Posts;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * @Route("/", name="dashboard")
     */
    public function index(PaginatorInterface $paginator, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $query = $entityManager->getRepository(Posts::class)->mostrarTodosPost();

        $user = $this->getUser();
        //obtengo los comentarios del post
        $comentariosDashboard = $entityManager->getRepository(Comentarios::class)->findBy(['user' => $user]);
        $comentariosDashboard = array_reverse($comentariosDashboard);
        $comentariosDashboard = array_slice($comentariosDashboard,0, 5);

        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            2
        );

        return $this->render('dashboard/index.html.twig', [
            'controller_name' => 'Bienvenido a tu Dashboard',
            'comentariosDashboard' => $comentariosDashboard,
            'pagination' => $pagination,
        ]);
    }
}
