<?php

namespace App\Controller;

use App\Entity\Comentarios;
use App\Entity\Posts;
use App\Form\ComentarioType;
use App\Form\PostsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PostsController extends AbstractController
{
    /**
     * @Route("/posts", name="nuevopost")
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $post = new Posts();
        $form = $this->createForm(PostsType::class, $post);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){


            $brochureFile = $form->get('fotos')->getData();

            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($brochureFile) {
                $originalFilename = pathinfo($brochureFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$brochureFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $brochureFile->move(
                        $this->getParameter('brochures_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    throw new \Exception('Error al subir la imagen');
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $post->setFotos($newFilename);
            }



            $user = $this->getUser();
            $post->setUser($user);


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($post);
            $entityManager->flush();
            $this->addFlash("exito", Posts::POST_CORRECTO);
            return $this->redirectToRoute('dashboard');
        }

        return $this->render('posts/index.html.twig', [
            'controller_name' => 'PostsController',
            'formulario'=> $form->createView()
        ]);
    }


    /**
     * @Route("/verpost/{id}", name="verpost")
     */
    public function verPost($id, Request $request){
        $entityManager = $this->getDoctrine()->getManager();
        //Obtengo el post actual
        $post = $entityManager->getRepository(Posts::class)->find($id);


        //obtengo los comentarios del post
        $comentariosPost = $entityManager->getRepository(Comentarios::class)->findBy(['posts' => $post->getId()]);


        //Creo el comentario
        $comentario = new Comentarios();
        $user = $this->getUser();
        //Le añado el usuario actual
        $comentario->setUser($user);
        $comentario->setPosts($post);

        $form = $this->createForm(ComentarioType::class, $comentario);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {

           // $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comentario);

            $entityManager->flush();
            $this->addFlash("exito", "comentario exitoso");
            return $this->redirectToRoute('dashboard');
        }



        return $this->render('posts/verPost.html.twig', [
            'post' => $post,
            'comentariosPost' => $comentariosPost,
            'formularioComentario' => $form->createView(),
        ]);
    }

    /**
     * @Route("/misposts", name="misposts")
     */
    public function misPosts(){
        $user = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();
        $posts = $entityManager->getRepository(Posts::class)->findBy(['user'=>$user]);

        return $this->render('posts/misPosts.html.twig', [
            'posts' => $posts,
        ]);
    }

    /**
     * @Route("/likes", options={"expose"=true}, name="likes")
     */
    public function likes(Request $request){

        if($request->isXmlHttpRequest()){
            $user = $this->getUser();
            $entityManager = $this->getDoctrine()->getManager();
            $id = $request->request->get('id');
            $post = $entityManager->getRepository(Posts::class)->find($id);
            $likes = $post->getLikes();
            $likes .= $user->getId().',';
            $post->setLikes($likes);
            $entityManager->flush();
            return new JsonResponse(['likes'=>$likes]);
        } else{
            throw new \Exception('Error en el tipo de solicitud');
        }



    }
}
