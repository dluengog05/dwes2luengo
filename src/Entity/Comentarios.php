<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ComentariosRepository")
 */
class Comentarios
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


   /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="comentarios")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Posts", inversedBy="comentarios")
     */
    private $posts;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $comentario;

    /**
     * @ORM\Column(type="date")
     */
    private $fecha_publicacion;

    /**
     * Comentarios constructor.
     * @param $user
     * @param $posts
     * @param $comentario
     * @param $fecha_publicacion
     */
    public function __construct($comentario = '')
    {
        $this->comentario = $comentario;
        $this->fecha_publicacion = new \DateTime();;
    }/**
 * @return mixed
 */
/**
 * @return mixed
 */
public function getUser()
{
    return $this->user;
}/**
 * @param mixed $user
 */
public function setUser($user): void
{
    $this->user = $user;
}



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComentario(): ?string
    {
        return $this->comentario;
    }

    public function setComentario(string $comentario): self
    {
        $this->comentario = $comentario;

        return $this;
    }

    public function getFechaPublicacion(): ?\DateTimeInterface
    {
        return $this->fecha_publicacion;
    }

    public function setFechaPublicacion(\DateTimeInterface $fecha_publicacion): self
    {
        $this->fecha_publicacion = $fecha_publicacion;

        return $this;
    }


    public function getPosts()
    {
        return $this->posts;
    }


    public function setPosts($posts): void
    {
        $this->posts = $posts;
    }


}
